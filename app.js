import express from 'express'
import cors from 'cors'
import pug from 'pug'
import morgan from 'morgan'
import favicon from 'serve-favicon'
import routes from './routes/index'
import routesItems from './routes/items'
/* Inicio - GraphQL */
import videos from './videos'
import { buildSchema } from 'graphql'
import graphqlHTTP from 'express-graphql'
const schema = buildSchema(`
    type Video{
        id: ID!
        title: String!
        views: Int
    }
    input VideoInput{
        title: String!
        views: Int
    }
    type Query{
        getVideos: [Video]
        getVideo(id: ID!): Video
    }
    type Mutation{
        addVideo( input: VideoInput ):Video
        updateVideo(id: ID!, input: VideoInput ): Video
        deleteVideo(id: ID!): String
    }
`)
const root = {
    getVideos(){
        return videos
    },
    getVideo({id}){
        console.log(id)
        return videos.find( v => v.id==id)        
    },
    addVideo({input}){
        const id = String(videos.length +1)
        const newVideo = {id,...input}
        videos.push(newVideo)
        return newVideo
    },
    updateVideo({id,input}){
        const index = videos.findIndex(e=> e.id===id)
        const video = videos[index]
        const editedVideo = Object.assign(video,input)
        videos[index] = editedVideo
        return editedVideo
    },
    deleteVideo({id}){
        const index = videos.findIndex(e=> e.id===id)
        videos.splice(1,index)
        return `Se elimino el video con id ${id}`
    }
}


/* Fin - GraphQL */
const app = express(),
    port = process.env.port || 3000,
    pathViews = `${__dirname}/views`,
    pathFavicon = `${__dirname}/public/img/favicon.ico`,
    pathPublic = `${__dirname}/public`;
/*funciones*/
function error404(req, res, next){
    //La funcion manda el error a la vista
    const error = new Error()
    const locals = {
        title: 'Error 404',
        descripcion: 'Recurso no encontrado',
        error
    }
    error.status = 404
    res.status(404).render('error404', locals)
}
app
    /* configuraciones de la App */
    .set('views', pathViews)    //Definimos la ruta para todas nuestra vistas (views)
    .set('view engine', 'pug')  //El motor de plantillas
    .set('port', port)  //Definimos el puerto a usar
    /* middlewares */
    .use('/graphql',graphqlHTTP({
        schema: schema,
        rootValue: root,
        graphiql: true
    }))
    .use(morgan('tiny'))    //para ver las peticiones a nuestro servidor
    .use(cors())    //para hacer peticiones a otros dominios
    .use(express.json())    //para usar json en respuestas
    .use(express.urlencoded({ extended: true }))   //para trabajar con aplicaiones www-form-urlencode
    .use(favicon(pathFavicon))  //para no tener problemas con el favicon
    .use(express.static(pathPublic))    //definimos la carpeta estatica (public)
    /* rutas */
    // .use('/', routes)
    // .use('/items', routesItems)
     /* 404 */
    .use(error404)
    .listen(port, () => {
        console.log(`Corriendo el el puerto: ${port}`)
    })